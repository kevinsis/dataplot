set term png font "arial"
set output "3dplot.png"
set title "Systems Report"
set xlabel "# cores"
set ylabel "cpu hours"
set zlabel "# jobs"
set logscale y
set grid

splot "axle_sorted.dat" u 2:1:3 with linespoints linetype 4 lw 3 title "Axle", "fuji_sorted.dat" u 2:1:3 with linespoints linetype 8 lw 3 title "Fuji"

set term x11
set out
replot
